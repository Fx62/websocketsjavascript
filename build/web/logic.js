//var person = prompt("Please enter your name");
// abrir el websocket, al ejecutarlo en red es necesario cambiar localhost por la ip del equipo donde se ejecute el proyecto
const webSocket = new WebSocket('ws://localhost:8080/byteslounge/websocket');
let count = 3;// la cuenta atras en segundo de la cuenta regresiva
let playerCount = 1;// El turno del jugador
let cID;//Id de la carta

// unicamente se ejecuta por al fallo o error inesperado
webSocket.onerror = function (event) {
    onError(event);
};

// unicamente se ejecuta una vez al abrir la pagina por cada usuario
webSocket.onopen = function (event) {
    //onOpen es otra funcion que lleva la logica de nueva sesion para comunicarse con java
    onOpen(event);
    document.getElementById('ressetPage').style.visibility="hidden";
};

// se ejecuta cada vez que se presione el boton de movement y tambien se debe de ejecutar al presionar el boton mazo
webSocket.onmessage = function (event) {
    onMessage(event);
    // onMessage se ejecuta multiples veces por sesio
    //alert(event.data);
};

// esta funcion es la principal y que lleva la mayor logica del juego
function onMessage(event) {
    // csv recibe un string separado por comas por parte de java, se parsean las comas y se guardan los valores en un array de javascript
    //alert(event.data);
    const csv = event.data.split(',');//Convierte el evento recibido en un arrayb que sera leido e interpretado con condicionales
    // para mostrar en consola los datos que envia java
    console.log(csv);
    // agrega la informacion de cada jugador conectado
    if (csv.length >= 10 && csv.length <= 14) {
        //Cada true recibido es que un jugador tiene cartas en su mazo. Es decir, está conectado
        if (csv[1] === 'true') {
            document.querySelector('#area1').innerHTML = '<b>Jugador 1<hr></b>' + csv[4] + ' cartas disponibles';
            //checkNumber(csv[4]);
        } else {
            document.querySelector('#area1').innerHTML = '';
        }
        if (csv[2] === 'true') {
            document.querySelector('#area2').innerHTML = '<b>Jugador 2<hr></b>' + csv[5] + ' cartas disponibles';
        } else {
            document.querySelector('#area2').innerHTML = '';
        }
        if (csv[3] === 'true') {
            document.querySelector('#area3').innerHTML = '<b>Jugador 3<hr></b>' + csv[6] + ' cartas disponibles';
        } else {
            document.querySelector('#area3').innerHTML = '';
        }
        // segun el numero de carta dibuja en el centro de la pagina una carta con canvas de html5
        let card = csv[7];
        let bg = '#ffffff;';//background
        let font = '';//color de texto
        switch (card.charAt(0)) {
            case 'A'://Red
                bg = '#8B0000';
                font = '#ffff00';
                break;
            case 'B'://
                bg = '#008000';
                font = '#FFD700';
                break;
            case 'C'://
                bg = '#191970';
                font = '#B0E0E6';
                break;
            case 'D'://
                bg = '#FFA500';
                font = '#FF4500';
                break;
            default:
                break;
        }//Añade el formato que se mostrará dentro del canvas. 
        const area = document.querySelector('#area');
        const areaContext = area.getContext("2d");
        areaContext.font = "bold 50px TimesNewRomans";
        areaContext.fillStyle = bg;//Se pinta la carta segun el switch
        areaContext.fillRect(0, 0, area.width, area.height);
        areaContext.fillStyle = font;
        areaContext.fillText(card.charAt(1), (area.width / 2) - 10, (area.height / 2) + 10);
        // red => bg = #8B0000 fn = #ffff00
        // green => bg = #008000 fn = #FFD700
        // blue => bg = #191970 fn = #B0E0E6
        // orange => bg = #FFA500 fn = #FF4500
        if (csv[8] !== '') {//Cantidad de cartas en el mazo
            document.getElementById('statistics').innerHTML = '<h3>' + csv[8] + ' cartas en el mazo</h3><hr>';
            // muestra la cantidad de jugadores conectados en la esquina superior izquierda
            document.getElementById('messages').innerHTML = csv[0];
        } else {
            document.getElementById('statistics').innerHTML = '<h2>' + csv[0] + '<h2>';
        }
        if (csv[1] === 'true' && csv[2] === 'true' && csv[3] === 'true') {
            //Comienza el juego. Los botones de move se habilitan, se limpian los espacios para empezar la partida
            document.getElementById('move').disabled = false;
            document.getElementById('errorPlayer').innerHTML = '';
            document.getElementById('ressetPage').style.visibility="hidden";
            playerCount = csv[10];//Turno del jugador
            document.getElementById('displayTurn').innerHTML = '<h5>Turno del jugador ' + playerCount + '<h5>';
        } else {//De lo contrario los botones se desabilitan
            document.getElementById('move').disabled = true;
            document.getElementById('acierto').disabled = true;
        }
        if (csv[11]) {
            cID = csv[11];//Recibe el id del juego.
            //console.log(cID);
        }
        if (csv[12] === 'Rol') {//Llama el metodo que cuenta hacia atras y desabilita el boton
            document.getElementById('acierto').disabled = true;
            displayCountdown();//Hola mamá soy un comentario
        }
        if (typeof csv[12] !== 'undefined') {//Despliega el jugador que perdio.
            if (csv[12].valueOf() !== 'Rol') {
                if (csv[12] !== '0') {
                    playerFail(csv[12]);//Llama el metodo junto con la variable, siendo este el jugador que ganó o perdió 
                }
            }
        }
        if (csv[13]) {//Reinicia los id del div y activa el boton
            document.getElementById('countdown').innerHTML = '';
            //document.getElementById('displayTurn').innerHTML = '';
            document.getElementById('counterID').innerHTML = '';
            document.getElementById('acierto').disabled = true;
        }
    }

}
;

// se ejecuta una sola vez por session
function onOpen(event) {
    //sendHand();
    //if (player) {
    //document.getElementById('messages').innerHTML = player;
    //}
}
;

// se ejecuta solo en caso de algun fallo
function onError(event) {
    alert(event.data);
}
;


// con esta funcion se envian informacion de java script a java
//Genera la variable y se manda el ID junto con la palabra HAND
function sendHand() {
    var acierto = document.getElementById('acierto');
    if (acierto !== null) {
        //webSocket.send(trig);
        webSocket.send(cID + 'hand');
        //console.log(cID);
    }
}
;

function reset() {
    webSocket.send("killAll");//Resettea el juego
    //cID = 0;
}
;

function iniTurno() {
    webSocket.send("turnoIniciado");//Manda al servidor java el trigger para iniciar el turno
}
;

function changeORG() {//Este se activa una vez la cuenta regresiva llega a 0
    //Despliega la informacion de que ID se jugara asi como el turno del jugador
    //Se habilita el boton del manotazo
//    if (playerCount === 3) {
//        playerCount = 1;
//    } else {
//        playerCount = playerCount + 1;
//    }
//    if (cID === 9) {
//        cID = 0;
//    } else {
//        cID = cID + 1;
//    }
    document.getElementById('countdown').innerHTML = 'YAAA!!!';
    document.getElementById('acierto').disabled = false;
    document.getElementById('displayTurn').innerHTML = '<h5>Turno del jugador ' + playerCount + '</h5>';
    document.getElementById('counterID').innerHTML = "Juega el ID: " + cID;
    //webSocket.send('playerSize');
    count = 3;
}
;

function displayCountdown() {
    //Cuenta atras
    if (count === 0) {//Si es igual a 0 manda a llamar el changeORG 
        changeORG();
        webSocket.send('card');
    } else {//Si es difetente a cero se ejecutara nuevamente cada segundo
        document.getElementById('counterID').innerHTML = '';
        document.getElementById('countdown').innerHTML = count;
        count = count - 1;
        setTimeout("displayCountdown()", 1000);
    }
}
;

function playerFail(playerLoser) {
    //Despliega la manera en la cual un jugador pierde o gana.
    if (playerLoser.valueOf() !== '') {
        if (playerLoser.length > 20) {
            document.getElementById('errorPlayer').innerHTML = playerLoser;// Despliega el jugador que ganó
            document.getElementById('ressetPage').style.visibility="visible";//Hace visible el boton de Resettear la partida
        } else {
            document.getElementById('errorPlayer').innerHTML = "Ha perdido el jugador: " + playerLoser;// Despliega el jugador que perdio
        }
    }
}
